import Category from "./Category.js";
import CategoryList from "./CategoryList.js";
import {storeInLocalStorage, getFromLocalStorage, removefromLocalStorage} from './Storage.js';
import Register from "./user.js";
let taskCounter: number = 1;


export class Task {
    id: number;
    title: string;
    description: string;
    priority: string;
    done: boolean;
    category: Category;

    constructor(title: string, description: string, priority: string, done: boolean, category: Category) {
        this.id = taskCounter++;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.done = done;
        this.category = category;
    }
}



let taskArr: Task[] = [];
const categoryArr: CategoryList = new CategoryList();
const UserRegister: Register = new Register();

let titelEl: HTMLInputElement;
let descriptionEl: HTMLInputElement;
let prioEl: HTMLInputElement;
let addTaskForm: HTMLFormElement;
let categoryTitleEl: HTMLInputElement;
let addCategoryForm: HTMLFormElement;
let loginForm: HTMLFormElement;
let username: HTMLInputElement;
let pass: HTMLInputElement;
let selectCategoryEl: HTMLSelectElement;
let orderTasksByDefaultBtn: HTMLButtonElement;
let orderTasksByCategoryBtn: HTMLButtonElement;
let viewTaskElement: HTMLElement;
let viewDoneTaskElement: HTMLElement;

let closeModalElement: HTMLElement;

document.addEventListener("DOMContentLoaded", () => {
    titelEl = document.getElementById("create-title") as HTMLInputElement;
    descriptionEl = document.getElementById("create-description") as HTMLInputElement;
    prioEl = document.getElementById("create-prio") as HTMLInputElement;
    addTaskForm = document.getElementById("create-task") as HTMLFormElement;
    addCategoryForm = document.getElementById("create-category") as HTMLFormElement;
    loginForm = document.getElementById("login") as HTMLFormElement;
    username = document.getElementById("user-name") as HTMLInputElement;
    pass = document.getElementById("password") as HTMLInputElement;
    categoryTitleEl = document.getElementById("create-category-title") as HTMLInputElement;
    selectCategoryEl = document.getElementById("select-category") as HTMLSelectElement;
    orderTasksByDefaultBtn = document.getElementById("orderTasksByDefaultBtn") as HTMLButtonElement;
    orderTasksByCategoryBtn = document.getElementById("orderTasksByCategoryBtn") as HTMLButtonElement;
    viewTaskElement = document.getElementById("view-task");
    viewDoneTaskElement = document.getElementById("view-done-task");

    closeModalElement = document.getElementById("closeModal") as HTMLElement;

    renderCategorySelect();
    renderTaskList();

    addTaskForm.addEventListener("submit", (e) => {
        e.preventDefault();
        addTask();
        addTaskForm.reset();
    });

    addCategoryForm.addEventListener("submit", (e) => {
        e.preventDefault();
        addCategory();
        addCategoryForm.reset();
    });

    loginForm.addEventListener("submit", (e) => {
        e.preventDefault();
        if(UserRegister.activeUser){
            UserRegister.logout();
            document.getElementById("login").innerHTML = "<div><div class=\"input-wrapper\"><label class=\"titel-label\" for=\"user-name\">user name</label><input class=\"custom-input\" type=\"text\" id=\"user-name\" name=\"user-name\" required> </div><div class=\"input-wrapper\"><label class=\"titel-label\" for=\"password\">password</label> <input class=\"custom-input\" type=\"text\" id=\"password\" name=\"password\" required></div></div><div><input class=\"form-button\" type=\"submit\" value=\"login\"></div><div>";
        }
        else{
            UserRegister.login(username.value, pass.value);
            document.getElementById("login").innerHTML = "<div> <label class=\"titel-label\" for=\"user-name\">"+UserRegister.activeUser.name+"</label></div><div><input class=\"form-button\" type=\"submit\" value=\"logout\"></div>";
        }    
    });

    orderTasksByDefaultBtn.addEventListener("click", (e) => {
        e.preventDefault();
        renderTaskList();
    });

    orderTasksByCategoryBtn.addEventListener("click", (e) => {
        e.preventDefault();
        renderTasksByCategory(-1)
    });

    closeModalElement.addEventListener("click", (e) => {
        e.preventDefault();
        closeEditTaskModal();
    });

    setStoredTasks();
    setStoredCategories();


});

/*-----------Tasks-----------*/
function setStoredTasks() {
    const storedTasks = getFromLocalStorage('tasks');
    if (storedTasks && Array.isArray(storedTasks)) {
        taskArr = storedTasks;
    }
    renderTaskList();
}

export function renderTaskList() {
    if (!viewTaskElement || !viewDoneTaskElement) {
        return;
    }

    viewTaskElement.innerHTML = "<h2>Open tasks</h2>";
    viewDoneTaskElement.innerHTML = "<h2>Completed tasks</h2>";

    taskArr.forEach((element) => {
        const taskContainer = document.createElement('div');
        taskContainer.classList.add('single-task');

        const taskContent = `
            <div class='task-tag'>
                <div class='task-prio ${element.priority}'></div>
                ${(element.category && element.category.title.length > 0 ? "<div class='task-cat'> " + element.category.title + "</div>" : "")}
            </div>
            <div class='task-title-label'>Title: </div>
            <div class='task-title'> ${element.title}</div>
            <div class='task-descr-label'>Description: </div>
            <div class='task-descr'>${element.description}</div>
            <button data-task-id="${element.id}" class="delete-task">Delete</button>
            <button data-task-id="${element.id}" class="done-task">Done</button>
            <button data-task-id="${element.id}" class="edit-task">Edit</button>
        `;

        if (element.done === false) {
            taskContainer.innerHTML = taskContent;

            taskContainer.querySelector('.done-task').addEventListener("click", () => {
                setTaskDone(element.id);
            });

            taskContainer.querySelector('.edit-task').addEventListener("click", () => {
                openEditTaskModal(element);
            });

            viewTaskElement.appendChild(taskContainer);
        } else {
            taskContainer.innerHTML = `
                <div>
                    Title: ${element.title} <br/>
                    Description: ${element.description} <br/>
                    Priority: ${element.priority} <br/>
                    ${(element.category && element.category.title.length > 0 ? "<div> Category: " + element.category.title + "</div>" : "")}
                    <br>
                    <button data-task-id="${element.id}" class="delete-task">Delete</button>
                    <button data-task-id="${element.id}" class="undone-task">Undone</button>
                </div>
            `;

            taskContainer.querySelector('.undone-task').addEventListener("click", () => {
                setTaskUndone(element.id);
            });

            viewDoneTaskElement.appendChild(taskContainer);
        }

        taskContainer.querySelector('.delete-task').addEventListener("click", () => {
            deleteTask(element.id);
        });

    });

    return true;
}


export function addTask() {
    const title = titelEl.value;
    const description = descriptionEl.value;
    const prio = prioEl.value;

    const categoryIndex = selectCategoryEl.selectedIndex;
    const category = categoryIndex !== -1 ? categoryArr.getCategory(categoryIndex - 1) : undefined;

    const task = new Task(title, description, prio, false, category);
    if (!verifyTask(task)) {
        console.log("Es müssen alle Felder ausgefüllt sein!");
        return
    }
    taskArr.push(task);
    console.log(taskArr);

    storeInLocalStorage('tasks', taskArr);

    renderTaskList();
}

function deleteTask(taksId: number) {
    let i = 0;
    while (i < taskArr.length) {
        if (taskArr[i].id === taksId) {
            taskArr.splice(i, 1);
        } else {
            ++i;
        }
    }
    removefromLocalStorage('tasks', taskArr)
    renderTaskList();
}

function openEditTaskModal(currentTask: Task) {
    const modal = document.getElementById("editTaskModal");

    console.log("Edit: " + currentTask);

    const modalEditTitle: HTMLInputElement = document.getElementById("editTitle") as HTMLInputElement;
    const modalEditDescription: HTMLInputElement = document.getElementById("editDescription") as HTMLInputElement;
    const modalEditPriority: HTMLInputElement = document.getElementById("editPriority") as HTMLInputElement;

    modalEditTitle.value = currentTask.title;
    modalEditDescription.value = currentTask.description;
    modalEditPriority.value = currentTask.priority;

    modal.style.display = "block";
}

//TODO Save Edit Task
/*
function saveEditTask(){
    console.log("save Task function.. coming soon");
    closeEditTaskModal();
    //refrash array
    //refreh local storage
    //render new list
}
*/

function closeEditTaskModal() {
    console.log("close Modal");
    const modal = document.getElementById("editTaskModal");
    modal.style.display = "none";
}

function setTaskDone(id: number) {
    for (const task of taskArr) {
        if (id === task.id) {
            task.done = true;
        }
    }
    removefromLocalStorage('tasks', taskArr)
    renderTaskList();
}

function setTaskUndone(id: number) {
    for (const task of taskArr) {
        if (id === task.id) {
            task.done = false;
        }
    }
    removefromLocalStorage('tasks', taskArr)
    renderTaskList();
}


function renderTasksByCategory(categoryId: number) {
    let selectedCategoryTitle: string;

    let filteredTasks: Task[];

    if (categoryId === -1) {
        selectedCategoryTitle = "No category";
        filteredTasks = taskArr.filter(task => !task.category);
    } else {
        const category = categoryArr.getCategory(categoryId);
        selectedCategoryTitle = category.title;
        filteredTasks = taskArr.filter(task => task.category && task.category.id === categoryId);
    }

    if (!viewTaskElement) {
        return;
    }

    viewTaskElement.innerHTML = "<h2>Tasks for category: " + selectedCategoryTitle + "</h2>";

    filteredTasks.forEach((element) => {
        const taskContainer = document.createElement('div');
        taskContainer.classList.add('single-task');

        const taskContent = `
            <div class='task-tag'>
                <div class='task-prio ${element.priority}'></div>
                ${(element.category && element.category.title.length > 0 ? "<div class='task-cat'> " + element.category.title + "</div>" : "")}
            </div>
            <div class='task-title-label'>Title: </div>
            <div class='task-title'> ${element.title}</div>
            <div class='task-descr-label'>Description: </div>
            <div class='task-descr'>${element.description}</div>
            <button data-task-id="${element.id}" class="delete-task">Delete</button>
            <button data-task-id="${element.id}" class="done-task">Done</button>
            <button data-task-id="${element.id}" class="edit-task">Edit</button>
        `;

        if (element.done === false) {
            taskContainer.innerHTML = taskContent;

            taskContainer.querySelector('.done-task').addEventListener("click", () => {
                setTaskDone(element.id);
            });

            taskContainer.querySelector('.edit-task').addEventListener("click", () => {
                openEditTaskModal(element);
            });

            taskContainer.querySelector('.delete-task').addEventListener("click", () => {
                deleteTask(element.id);
            });

            viewTaskElement.appendChild(taskContainer);
        }
    });
}



/*---------Category----------*/

function setStoredCategories() {
    const storedCategoriesData = getFromLocalStorage('categories');
    if (storedCategoriesData && storedCategoriesData.categories && Array.isArray(storedCategoriesData.categories)) {
        const storedCategories = storedCategoriesData.categories;
        categoryArr.setCategories(storedCategories);
    }
    renderCategorySelect();
    renderAllCategories();
}

function addCategory() {
    const title = categoryTitleEl.value;
    const category = new Category(title);

    categoryArr.addCategory(category);
    console.log(categoryArr.getCategories());

    storeInLocalStorage('categories', categoryArr);

    renderCategorySelect();
    renderAllCategories();
}

function renderCategorySelect() {
    const selectCategoryEl = document.getElementById("select-category") as HTMLSelectElement;
    if (!selectCategoryEl) {
        return;
    }
    selectCategoryEl.innerHTML = "";
    selectCategoryEl.innerHTML += '<option value="-1" selected>No category</option>';
    for (const category of categoryArr.getCategories()) {
        selectCategoryEl.innerHTML += `<option value="${category.id}">${category.title}</option>`;
    }
}

function renderAllCategories() {
    const viewCategoriesElement = document.getElementById("view-categories");

    if (!viewCategoriesElement) {
        return;
    }

    viewCategoriesElement.innerHTML = "";

    categoryArr.getCategories().forEach((category) => {
        const categoryContainer = document.createElement('div');
        categoryContainer.classList.add('single-category');

        categoryContainer.innerHTML = `
            <div id="categoryOrder-${category.id}" class='category-title'>
                ${category.title}
            </div>
            <i class="gg-trash" id='deleteCategory-${category.id}'></i>
        `;

        categoryContainer.querySelector(`#categoryOrder-${category.id}`).addEventListener("click", () => {
            renderTasksByCategory(category.id);
            console.log("ordered by category: " + category.title);
        });

        categoryContainer.querySelector(`#deleteCategory-${category.id}`).addEventListener("click", () => {
            deleteCategory(category.id);
            renderAllCategories();
        });

        viewCategoriesElement.appendChild(categoryContainer);
    });
}


function deleteCategory(categoryId: number) {
    const deleted = categoryArr.deleteCategory(categoryId);
    if (deleted) {
        removefromLocalStorage('categories', categoryArr);
        console.log("Category deleted:", categoryId);
        renderCategorySelect();
        renderAllCategories();
    } else {
        console.log("Category could not b deleted:", categoryId);

    }

}

function verifyTask(task: Task) {
    if (task.title.trim().length === 0 || task.description.trim().length === 0 || task.priority.trim().length === 0) {
        return false;
    }
    return true;
}

