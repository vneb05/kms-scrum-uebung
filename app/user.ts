export let userCounter: number = 0;
export class User {
    id: number;
    name: string;
    password: string;

    constructor(name: string, pass: string) {
        this.id = userCounter;
        this.name = name;
        this.password = pass;
        userCounter++;
    }
}
export default class Register{
    private Users: User[];
    public activeUser: User;

    constructor(){
        this.Users = [];
    }

    getUsers(): User[]{
        return this.Users;
    }

    public login(name: string, pass: string) {
        
        for(let i=0 ; i< this.Users.length; i++){
            if(this.Users[i].name=== name && this.Users[i].password === pass) this.activeUser = this.Users[i];
        }
        
        
    }
    public logout() {
        this.activeUser = null;
        
    }
    
    public register(name: string, pass: string) {
        console.log("register "+ name);
        console.log(this.Users);
        this.Users.push(new User(name,pass));
        this.login(name,pass);
    }
    
}