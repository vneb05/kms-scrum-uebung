import Category from "../app/Category";

/**
 * Represents a task.
 */
export default class Task {
    /**
     * The unique identifier of the task.
     * @type {number}
     */
    id: number;

    /**
     * The title of the task.
     * @type {string}
     */
    title: string;

    /**
     * The description of the task.
     * @type {string}
     */
    description: string;

    /**
     * The priority level of the task.
     * @type {string}
     */
    priority: string;

    /**
     * Indicates whether the task is done or not.
     * @type {boolean}
     */
    done: boolean;

    /**
     * The category to which the task belongs.
     * @type {Category}
     */
    category: Category;

    /**
     * Counter for assigning unique IDs to tasks.
     * @type {number}
     * @static
     */
    static countTasks = 0;
  
    /**
     * Creates a new Task instance.
     * @param {string} title - The title of the task.
     * @param {string} description - The description of the task.
     * @param {string} priority - The priority level of the task.
     * @param {boolean} done - Indicates whether the task is done or not.
     * @param {Category} category - The category to which the task belongs.
     */
    constructor(title: string, description: string, priority: string, done: boolean, category: Category) {
      /**
         * The unique identifier of the task.
         * @type {number}
         */
      this.id = Task.countTasks++;

      /**
         * The title of the task.
         * @type {string}
         */
      this.title = title;

      /**
         * The description of the task.
         * @type {string}
         */
      this.description = description;

      /**
         * The priority level of the task.
         * @type {string}
         */
      this.priority = priority;

      /**
         * Indicates whether the task is done or not.
         * @type {boolean}
         */
      this.done = done;

      /**
         * The category to which the task belongs.
         * @type {Category}
         */
      this.category = category;
    }

    /**
     * Verifies if the task is valid.
     * @returns {boolean} True if the task is valid, false otherwise.
     */
    verifyTask(): boolean {
      if (this.title.trim().length === 0 || this.description.trim().length === 0 || this.priority.trim().length === 0) {
        return false;
      }
      return true;
      }
}