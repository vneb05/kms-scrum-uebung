/**
 * Represents a category.
 */
export default class Category {
    /**
     * Counter for assigning unique IDs to categories.
     * @type {number}
     * @static
     */
    static categoryCounter = 0;

    /**
     * Unique identifier for the category.
     * @type {number}
     */
    id: number;

    /**
     * Title of the category.
     * @type {string}
     */
    title: string;

    /**
     * Creates a new Category instance.
     * @param {string} title - The title of the category.
     */
    constructor(title: string) {
        /**
         * Unique identifier for the category.
         * @type {number}
         */
        this.id = Category.categoryCounter++;

        /**
         * Title of the category.
         * @type {string}
         */
        this.title = title;
    }
}