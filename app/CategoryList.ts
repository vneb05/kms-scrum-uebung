
import Category from "./Category";

/**
 * Represents a list of categories.
 */
export default class CategoryList {
    /**
     * Array of categories.
     * @type {Category[]}
     * @private
     */
    private categories: Category[];

     /**
     * Creates a new CategoryList instance.
     */
    constructor() {
         /**
         * Array of categories.
         * @type {Category[]}
         */
        this.categories = [];
    }

    /**
     * Retrieves all categories.
     * @returns {Category[]} The array of categories.
     */
    getCategories(): Category[] {
        return this.categories;
    }

    /**
     * Adds a new category to the list.
     * @param {Category} category - The category to add.
     */
    addCategory(category: Category) {
        this.categories.push(category);
    }

    /**
     * Deletes a category by its ID from the list.
     * @param {number} id - The ID of the category to delete.
     * @returns {boolean} True if deletion was successful, false otherwise.
     */
    deleteCategory(id: number): boolean {
        for (let i = 0; i < this.categories.length; i++) {
            if (this.categories[i].id == id) {
                this.categories.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves a category by its ID.
     * @param {number} id - The ID of the category to retrieve.
     * @returns {Category | null} The category if found, otherwise null.
     */
    getCategory(id: number): Category | null {
        for (const category of this.categories) {
            if (id === category.id) {
                return category;
            }
        }
        return null;
    }

    /**
     * Sets categories to a new array of categories.
     * @param {Category[]} newCategories - The new array of categories.
     */
    setCategories(newCategories: Category[]) {
        this.categories = newCategories;
    }

    /**
     * Edits the title of a category.
     * @param {number} id - The ID of the category to edit.
     * @param {string} title - The new title for the category.
     * @returns {boolean} True if editing was successful, false otherwise.
     */
    editCategory(id: number, title: string): boolean {
        for (const category of this.categories) {
            if (category.id === id) {
                category.title = title;
                return true;
            }
        }
        return false;
    }

}