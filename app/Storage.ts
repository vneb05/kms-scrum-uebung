/**
 * Stores a value in the local storage.
 * @param {string} key - The key under which to store the value.
 * @param {*} value - The value to store.
 */
export function storeInLocalStorage(key: string, value) {
    window.localStorage.setItem(key, JSON.stringify(value));
}

/**
 * Removes a value from the local storage.
 * @param {string} key - The key of the value to remove.
 * @param {*} value - The value to remove.
 */
export function removefromLocalStorage(key: string, value) {
    window.localStorage.setItem(key, JSON.stringify(value));
}

/**
 * Retrieves a value from the local storage.
 * @param {string} key - The key of the value to retrieve.
 * @returns {*} The retrieved value, parsed from JSON.
 */
export function getFromLocalStorage(key: string) {
    return JSON.parse(window.localStorage.getItem(key))
}

