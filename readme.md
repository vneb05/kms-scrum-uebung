KMS WiSe 2023
# To-Do-Liste-App

## Gruppe 2
Von Anastasia, Elisa, Jimmy, Jonas, Julius, Steffen, Vincent

## Starten
Die seite ist deployt auf https://vneb05.git-pages.thm.de/kms-scrum-uebung/

Sollte es einen Defekt geben lässt sich das Projekt alternativ wie folgt starten:
1. Herunterladen (Clone oder Archiv)
2. Terminal im entsprechenden Verzeichnis öffnen und `docker build -t todo-app .` ausführen um das image zu bauen
3. Schließlich den container mit `docker run -d -p 8000:80 --name todo-app todo-app` starten
4. Die Seite ist unter "http://localhost:8000/" einsehbar
5. Container mit `sudo docker stop todo-app` stoppen

Die Unit-Tests können folgendermaßen ausgeführt werden:
1. Herunterladen (Clone oder Archiv)
2. Terminal im entsprechenden Verzeichnis öffnen und `npm install` ausführen
3. `npm run test` um die tests auszuführen


## Features
>### Task
> Create Task (Add Priority and Category)
> 
> Sort Tasks by Categories
> 
> Mark Tasks as Done/Undone
> 
> Delete Task
> 
> Tasks are stored/delete in Local Storage


> ### Category
> Create Category
> 
> Delete Category
> 
> Categories are stored/delete in Local Storage

> ### User
> 
