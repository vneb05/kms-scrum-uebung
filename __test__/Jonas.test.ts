import {describe, expect, test} from '@jest/globals';
import Task from "../app/Task";
import Category from '../app/Category';

describe( 'Task', () => {
    const category = new Category("Test Category");
    //Should return true because Task is valid
    test('Check valid Task', () => {
        const task: Task = new Task("Test Task", "Example Description", "High", false, category);
        expect(task.verifyTask()).toBe(true);
    });

    //Should return false because Task is invalid (Description empty)
    test('Check invalid Task', () => {
        const task: Task = new Task("Test Task", "", "Middle", true, category);
        expect(task.verifyTask()).toBe(false);
    });

});