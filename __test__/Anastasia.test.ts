import {describe, expect, test} from '@jest/globals';
import Category from "../app/Category";
import CategoryList from "../app/CategoryList";


describe('CategoryList', () => {

    //Should be returned true because the category exist and can be deleted
    test('delete existing category', () => {
        const categoryList = new CategoryList();
        const category = new Category('Example Category');
        categoryList.addCategory(category);

        const isDeleted = categoryList.deleteCategory(category.id);

        expect(isDeleted).toBe(true);
        const allCategories = categoryList.getCategories();
        expect(allCategories).not.toContain(category);
    });

    //Should return false because the category does not exist and cant be deleted
    test('delete non-existing category', () => {
        const categoryList = new CategoryList();
        const isDeleted = categoryList.deleteCategory(111111);
        expect(isDeleted).toBe(false);
    });
});