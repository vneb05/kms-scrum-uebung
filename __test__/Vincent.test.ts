import {describe, expect, test} from '@jest/globals';
import Category from "../app/Category";
import CategoryList from "../app/CategoryList";

describe('CategoryList', () => {
    test('create CategoryList', () => {
        const categoryList = new CategoryList();
        const arr = categoryList.getCategories();
        expect(arr.length).toBe(0);
    });

    test('add a category', () => {
        const categoryList = new CategoryList();
        categoryList.addCategory(new Category("Hallo"))
        const arr = categoryList.getCategories();
        expect(arr.length).toBe(1);
    });


});
