import {describe, expect, test} from '@jest/globals';
import Task from "../app/Task";
import Category from '../app/Category';


describe( 'Task', () => {
    const category = new Category("Test Category");

    test('Assign task to category', () => {
        const newCategory = new Category('New Category');
        const task = new Task('Title', 'Description', 'Low', false, category);
        task.category = newCategory;
        expect(task.category).toBe(newCategory);
    });


    test('Ensure unique IDs for tasks', () => {
        const task1 = new Task('Title 1', 'Description', 'Low', false, category);
        const task2 = new Task('Title 2', 'Description', 'Medium', false, category);
        expect(task1.id).not.toBe(task2.id);
    });

});
