import {describe, expect, test} from '@jest/globals';
import Category from "../app/Category";

describe('Category', () => {
    test('category counter increases', () => {
        expect(Category.categoryCounter).toBe(0);
        new Category('Example Category');
        expect(Category.categoryCounter).toBe(1);
    });

    test('category title is set', () => {
        const title = 'Example Category';
        const category = new Category(title);
        expect(category.title).toBe(title);
    });
});