import {describe, expect, test} from '@jest/globals';
import Task from "../app/Task";
import Category from '../app/Category';
import CategoryList from "../app/CategoryList";

describe( 'Task', () => {

    test('checking Index of Tasks', () => {
        const testCategory = new Category("JestTests");
        const test1 = new Task("Test1", "First Task", "High", false, testCategory);
        const test2 = new Task("Test2", "2nd Task", "Middle", true, testCategory);
        expect(test1.id).toBe(0);
        expect(test2.id).toBe(1);
    });

});

describe( 'CategoryList', () => {

    test('Choosing right Category of CategoryList', () => {
        const testCategoryList = new CategoryList();
        testCategoryList.addCategory(new Category("Test1"));
        testCategoryList.addCategory(new Category("Test2"));
        expect(testCategoryList.getCategory(1)?.id).toBe(1);
        expect(testCategoryList.getCategory(2)?.id).toBe(2);
        expect(testCategoryList.getCategory(1)?.title).toBe("Test1");
        expect(testCategoryList.getCategory(2)?.title).toBe("Test2");
    });

});
