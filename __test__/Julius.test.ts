import {describe, expect, test} from '@jest/globals';
import Category from '../app/Category';

import Register from '../app/user';

describe( 'Login', () => {
    let reg = new Register;
    //Should match since new user has just been registered
    test('Check Register', () => {//check positiv
        reg = new Register;
        reg.register("test", "test")
        expect(reg.activeUser.name).toBe("test");
        reg.logout();

        reg.register("test1", "test1")
        expect(reg.activeUser.name).toBe("test1");
        reg.logout();

        reg.register("test2", "test2")
        expect(reg.activeUser.name).toBe("test2");
        reg.logout();
    });
    test('Check Logout', () => {
        reg.logout();
        expect(reg.activeUser).toBe(null);
    });
    test('Check Login', () => {//check positiv
        reg = new Register();
        reg.register("test", "test")
        reg.logout();
        reg.register("test1", "test1")
        reg.logout();
        reg.register("test2", "test2")
        reg.logout();

        reg.login("test", "test")
        expect(reg.activeUser.name).toBe("test");
        reg.logout();
        reg.login("test1", "test1")
        expect(reg.activeUser.name).toBe("test1");
        reg.logout();
        reg.login("test2", "test2")
        expect(reg.activeUser.name).toBe("test2");
    });
    test('Check Login', () => {//check negative
        reg = new Register();
        reg.register("test", "test")
        reg.logout();
        reg.register("test1", "test1")
        reg.logout();
        reg.register("test2", "test2")
        reg.logout();

        reg.login("test", "test111")
        expect(reg.activeUser).toBe(null);
        reg.logout();
        reg.login("test1", "test33")
        expect(reg.activeUser).toBe(null);
        reg.logout();
        reg.login("test2", "test42")
        expect(reg.activeUser).toBe(null);
    });
    

});